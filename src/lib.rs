#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_transform_a_compact_raw_json_to_a_human_readable_json() {
    let raw_json = r#"{"name":"John","age":30,"cars":{"car1":"Ford","car2":"BMW","car3":"Fiat"}}"#;
    let human_readable_json = r#"
{
  "name":"John",
  "age":30,
  "cars": {
    "car1":"Ford",
    "car2":"BMW",
    "car3":"Fiat"
  }
}"#;

        assert_eq!(transform_to_human_readable_json(&raw_json), human_readable_json);
    }
}
